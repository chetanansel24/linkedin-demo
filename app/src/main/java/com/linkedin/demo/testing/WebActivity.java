package com.linkedin.demo.testing;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.model.Token;
import com.model.UserModel;
import com.viewmodel.LinkedinUserViewModel;
import com.viewmodel.WebViewModel;

import java.util.Objects;

import static com.linkedin.demo.testing.Utils.API_KEY;
import static com.linkedin.demo.testing.Utils.REDIRECT_URI;
import static com.linkedin.demo.testing.Utils.RESPONSE_TYPE_VALUE;
import static com.linkedin.demo.testing.Utils.SECRET_KEY;
import static com.linkedin.demo.testing.Utils.STATE;
import static com.linkedin.demo.testing.Utils.STATE_PARAM;
import static com.linkedin.demo.testing.Utils.getAuthorizationUrl;

public class WebActivity extends AppCompatActivity {
    private static final String GRANT_TYPE = "authorization_code";
    private WebView webView;
    private WebViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);
        webView = findViewById(R.id.activity_web_view);
        viewModel = ViewModelProviders.of(this).get(WebViewModel.class);
        webView.requestFocus(View.FOCUS_DOWN);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String authorizationUrl) {
                if (authorizationUrl.startsWith(REDIRECT_URI)) {
                    Log.i("Authorize", "");
                    Uri uri = Uri.parse(authorizationUrl);
                    String stateToken = uri.getQueryParameter(STATE_PARAM);
                    if (stateToken == null || !stateToken.equals(STATE)) {
                        Log.e("Authorize", "State token doesn't match");
                        return true;
                    }

                    String authorizationToken = uri.getQueryParameter(RESPONSE_TYPE_VALUE);
                    if (authorizationToken == null) {
                        Log.i("Authorize", "The user doesn't allow authorization.");
                        return true;
                    }
                    Log.i("Authorize", "Auth token received: " + authorizationToken);

                    viewModel.getToken(GRANT_TYPE, authorizationToken, API_KEY, REDIRECT_URI,
                            SECRET_KEY).observe(WebActivity.this,
                            new Observer<Token>() {
                                @Override
                                public void onChanged(@Nullable Token token) {
                                    String tokenAccess = Objects.requireNonNull(token).getAccessToken();
                                    Log.d("Token", "" + tokenAccess);
                                    if (Integer.valueOf(token.getExpiresIn()) > 0 &&
                                            tokenAccess != null) {
                                        ViewModelProviders.of(WebActivity.this).get
                                                (LinkedinUserViewModel.class).getUserDetails
                                                (tokenAccess).observe(WebActivity.this,
                                                new Observer<UserModel>() {
                                            @Override
                                            public void onChanged(@Nullable UserModel linkedInUserModel) {
                                                Log.d("lastname", "" + Objects.requireNonNull(linkedInUserModel).
                                                        getmFirstName());
                                                Intent resultIntent = new Intent();
                                                resultIntent.putExtra("result",
                                                        linkedInUserModel);
                                                setResult(RESULT_OK, resultIntent);
                                                finish();

                                            }
                                        });


                                    }
                                }
                            });

                } else {
                    //Default behaviour
                    Log.i("Authorize", "Redirecting to: " + authorizationUrl);
                    webView.loadUrl(authorizationUrl);
                }
                return true;
            }
        });

        String authUrl = getAuthorizationUrl();
        Log.i("Authorize", "Loading Auth Url: " + authUrl);
        webView.loadUrl(authUrl);

    }


}
