package com.linkedin.demo.testing;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.model.UserModel;

public class MainActivity extends AppCompatActivity {

    private static final int CODE = 1001;
    private TextView mTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }


    private void initViews() {
        Button signInButton = findViewById(R.id.linkedin_login_button);
        mTextView = findViewById(R.id.answer);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WebActivity.class);
                startActivityForResult(intent, CODE);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE) {

            if (resultCode == RESULT_OK) {
                UserModel userModel = (UserModel) data.
                        getSerializableExtra("result");

                String firstName = userModel.getmFirstName();
                String lastName = userModel.getmFLastName();
                String emailAddress = userModel.getmEmailAddress();
                mTextView.setText("FirstName : " + firstName + "  " + "LastName : " + lastName
                        + "  EmailAdress : " + emailAddress + " id : " +userModel.getmId() + "  profile Url:  "
                +userModel.getmProfileUrl());
            }

        }

    }
}
