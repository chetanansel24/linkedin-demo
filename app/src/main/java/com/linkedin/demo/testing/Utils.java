package com.linkedin.demo.testing;

class Utils {

    //Add your Dev account API key
    public static final String API_KEY = "81ymqreqz8cuvl";
    //ADD your dev account secret key
    public static final String SECRET_KEY = "kdGmCn9vTN03K2nP";
    //Add ur own state
    public static final String STATE = "123456789";

    //Add your redirect URI
    public static final String REDIRECT_URI = "https://example.com";

    private static final String AUTHORIZATION_URL = "https://www.linkedin.com/oauth/v2/authorization";
    private static final String RESPONSE_TYPE_PARAM = "response_type";
    public static final String RESPONSE_TYPE_VALUE = "code";
    private static final String CLIENT_ID_PARAM = "client_id";
    public static final String STATE_PARAM = "state";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    /*---------------------------------------*/
    private static final String QUESTION_MARK = "?";
    private static final String AMPERSAND = "&";
    private static final String EQUALS = "=";


    public static String getAuthorizationUrl() {
        return AUTHORIZATION_URL
                + QUESTION_MARK + RESPONSE_TYPE_PARAM + EQUALS + RESPONSE_TYPE_VALUE
                + AMPERSAND + CLIENT_ID_PARAM + EQUALS + API_KEY
                + AMPERSAND + REDIRECT_URI_PARAM + EQUALS + REDIRECT_URI
                + AMPERSAND + STATE_PARAM + EQUALS + STATE
                + AMPERSAND + "scope=r_liteprofile%20r_emailaddress";
    }
}
