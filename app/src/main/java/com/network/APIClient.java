package com.network;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.model.Token;
import com.model.EmailAddressModel;
import com.model.LinkedInUserModel;
import com.model.UserModel;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    private static APIClient apiClient = null;
    private static final String LINKEDIN_URL = "https://api.linkedin.com/";

    private Retrofit getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        return new Retrofit.Builder()
                .baseUrl("https://www.linkedin.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static APIClient getInstance() {
        if (apiClient == null) {
            apiClient = new APIClient();
        }
        return apiClient;
    }

    @Nullable
    public LiveData<Token> getToken(String grantType, String responseType,
                             String clientId,
                             String redirectUri,
                             String secretKey) {
        final MutableLiveData<Token> userData = new MutableLiveData<>();

        APIInterface apiInterface = getClient().create(APIInterface.class);
        apiInterface.getToken(grantType, responseType, clientId, redirectUri, secretKey).
                enqueue(new Callback<Token>() {
                    @Override
                    public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token>
                            response) {
                        Token body = response.body();

                        if (body == null) {
                            userData.setValue(null);
                            Log.d("error", "body is null");

                        } else {
                            userData.setValue(body);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                        Log.d("onFailure", t.getLocalizedMessage());
                        userData.setValue(null);
                    }
                });


        return userData;
    }


    public LiveData<UserModel> getUserDetails(final String accessToken) {
        final MutableLiveData<UserModel> userData = new MutableLiveData<>();

        APIInterface apiInterface = getLinkedinProfile(accessToken).create(APIInterface.class);
        apiInterface.getUserDetails().
                enqueue(new Callback<LinkedInUserModel>() {
                    @Override
                    public void onResponse(@NonNull Call<LinkedInUserModel> call, @NonNull Response<LinkedInUserModel>
                            response) {
                        final LinkedInUserModel body = response.body();

                        if (body == null) {
                            return;
                        } else {
                            Log.d("response",""+body.toString());
                            APIInterface apiInterface = getLinkedinProfile(accessToken).create(APIInterface.class);
                            apiInterface.getUserEmailDetails().enqueue(new Callback<EmailAddressModel>() {
                                @Override
                                public void onResponse(Call<EmailAddressModel> call,
                                                       Response<EmailAddressModel> response) {
                                    EmailAddressModel emailAddressModels = response.body();
                                    if (emailAddressModels != null) {
                                        Log.d("email Adderess", "" +
                                                emailAddressModels.getHandleModel().get(0).getHandleModel().getEmailAddress());

                                        String id = body.getId();
                                        String profileUrl = body.getProfilePicture().
                                                getDisplayImage().getProfileElementsModels().get(2).
                                                getProfileIdentifiersModels().get(0).getmIdentifier();
                                        UserModel userModel = new UserModel(body.
                                                getFirstNameModel().getLocalizedLastName().
                                                getLastName(), body.getLastNameModel().
                                                getLocalizedLastName().getLastName(),
                                                emailAddressModels.getHandleModel().get(0).
                                                        getHandleModel().getEmailAddress(),profileUrl,id);
                                        userData.setValue(userModel);
                                    }

                                }

                                @Override
                                public void onFailure(Call<EmailAddressModel> call, Throwable t) {
                                    Log.d("onFailure", "" +
                                            t.getLocalizedMessage());
                                }
                            });

                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<LinkedInUserModel> call, @NonNull Throwable t) {
                        Log.d("onFailure", "" +
                                t.getLocalizedMessage());
                        return;

                    }
                });


        return userData;
    }


    private Retrofit getLinkedinProfile(final String value) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(AUTHORIZATION, BEARER + value)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        return new Retrofit.Builder()
                .baseUrl(LINKEDIN_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

}
