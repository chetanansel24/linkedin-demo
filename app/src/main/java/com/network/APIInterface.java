package com.network;

import com.model.Token;
import com.model.EmailAddressModel;
import com.model.LinkedInUserModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("uas/oauth2/accessToken")
    Call<Token> getToken(@Query("grant_type") String grantType, @Query("code") String responseType,
                         @Query("client_id") String clientId,
                         @Query("redirect_uri") String redirectUri,
                         @Query("client_secret") String secretKey);

    @GET("v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))")
    Call<LinkedInUserModel> getUserDetails();

    @GET("v2/emailAddress?q=members&projection=(elements*(handle~))")
    Call<EmailAddressModel> getUserEmailDetails();

}
