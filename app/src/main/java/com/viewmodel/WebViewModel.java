package com.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;


import com.model.Token;
import com.network.APIClient;

public class WebViewModel extends ViewModel {

    public LiveData<Token> getToken(String grantType, String responseType,
                                    String clientId,
                                    String redirectUri,
                                    String secretKey) {
        return APIClient.getInstance().getToken(grantType, responseType,
                clientId,
                redirectUri,
                secretKey);
    }
}
