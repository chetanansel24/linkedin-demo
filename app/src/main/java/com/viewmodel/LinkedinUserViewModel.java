package com.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;


import com.model.UserModel;
import com.network.APIClient;

public class LinkedinUserViewModel extends ViewModel {

    public LiveData<UserModel> getUserDetails(String grantType) {
        return APIClient.getInstance().getUserDetails(grantType);
    }
}
