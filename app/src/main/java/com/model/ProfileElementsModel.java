package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProfileElementsModel implements Serializable {


    @SerializedName("identifiers")
    private List<ProfileIdentifiersModel> profileIdentifiersModels;

    public List<ProfileIdentifiersModel> getProfileIdentifiersModels() {
        return profileIdentifiersModels;
    }

    @Override
    public String toString() {
        return "ProfileElementsModel{" +
                "profileIdentifiersModels=" + profileIdentifiersModels +
                '}';
    }
}
