package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfilePicture implements Serializable {

    @SerializedName("displayImage~")
    private DisplayImage displayImage;

    public DisplayImage getDisplayImage() {
        return displayImage;
    }

    @Override
    public String toString() {
        return "ProfilePicture{" +
                "displayImage=" + displayImage +
                '}';
    }
}
