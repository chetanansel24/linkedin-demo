package com.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmailAddressModel {

    @SerializedName("elements")
    private List<ElementsModel> mHandleModel;

    public List<ElementsModel> getHandleModel() {
        return mHandleModel;
    }
}
