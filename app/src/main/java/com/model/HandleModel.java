package com.model;

import com.google.gson.annotations.SerializedName;

public class HandleModel {

    @SerializedName("emailAddress")
    private String mEmailAddress;

    public String getEmailAddress() {
        return mEmailAddress;
    }
}
