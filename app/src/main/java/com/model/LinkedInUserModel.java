package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LinkedInUserModel implements Serializable {

    @SerializedName("id")
    private String mId;

    @SerializedName("lastName")
    private LastNameModel lastNameModel;

    public String getId() {
        return mId;
    }

    public LastNameModel getLastNameModel() {
        return lastNameModel;
    }

    @SerializedName("firstName")
    private FirstNameModel firstNameModel;

    public FirstNameModel getFirstNameModel() {
        return firstNameModel;
    }

    @SerializedName("profilePicture")
    private ProfilePicture profilePicture;

    public ProfilePicture getProfilePicture() {
        return profilePicture;
    }

    @Override
    public String toString() {
        return "LinkedInUserModel{" +
                "mId='" + mId + '\'' +
                ", lastNameModel=" + lastNameModel +
                ", firstNameModel=" + firstNameModel +
                ", profilePicture=" + profilePicture +
                '}';
    }
}
