package com.model;

import com.google.gson.annotations.SerializedName;

public class Token {

    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("expires_in")
    private String mExpiresIn;

    public String getAccessToken() {
        return mAccessToken;
    }

    public String getExpiresIn() {
        return mExpiresIn;
    }
}
