package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LastNameModel implements Serializable {

        @SerializedName("localized")
        private LocalizedName localizedLastName;

    public LocalizedName getLocalizedLastName() {
        return localizedLastName;
    }
}
