package com.model;

import java.io.Serializable;

public class UserModel implements Serializable {

    private String mFirstName;
    private String mFLastName;
    private String mEmailAddress;
    private String mProfileUrl;
    private String mId;

    public UserModel(String mFirstName, String mFLastName, String mEmailAddress, String profileUrl,
                     String id) {
        this.mFirstName = mFirstName;
        this.mFLastName = mFLastName;
        this.mEmailAddress = mEmailAddress;
        mProfileUrl = profileUrl;
        mId = id;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public String getmFLastName() {
        return mFLastName;
    }

    public String getmEmailAddress() {
        return mEmailAddress;
    }

    public String getmProfileUrl() {
        return mProfileUrl;
    }

    public String getmId() {
        return mId;
    }
}
