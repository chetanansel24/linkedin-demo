package com.model;

import com.google.gson.annotations.SerializedName;

public class ElementsModel {
    @SerializedName("handle~")
    private HandleModel mHandleModel;

    public HandleModel getHandleModel() {
        return mHandleModel;
    }
}
