package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LocalizedName implements Serializable {


    @SerializedName("en_US")
    private String mLastName;

    public String getLastName() {
        return mLastName;
    }
}
