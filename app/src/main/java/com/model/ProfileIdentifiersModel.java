package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileIdentifiersModel implements Serializable {

    @SerializedName("identifier")
    private String mIdentifier;

    public String getmIdentifier() {
        return mIdentifier;
    }

    @Override
    public String toString() {
        return "ProfileIdentifiersModel{" +
                "mIdentifier='" + mIdentifier + '\'' +
                '}';
    }
}
