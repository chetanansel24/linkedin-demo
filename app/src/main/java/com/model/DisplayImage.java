package com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DisplayImage implements Serializable {

    @SerializedName("elements")
    private List<ProfileElementsModel> profileElementsModels;

    public List<ProfileElementsModel> getProfileElementsModels() {
        return profileElementsModels;
    }

    @Override
    public String toString() {
        return "DisplayImage{" +
                "profileElementsModels=" + profileElementsModels +
                '}';
    }
}
