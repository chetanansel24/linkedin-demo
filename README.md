LinkedIn Login Android App using Webview OAuth 2.0 (V2) API without using Linkedin SDK.

Now User can able to login in Linkedin using this App Code.

App will display user first Name, last name and Email Address.

In App code, you just nee dto change the API key, Secret key and Redirect URI in Utils.java.

In Code, Parsing the response and network Call all are done.

Below is the Utils.Java :

package com.linkedin.demo.testing;

public class Utils {

    //Add your Dev account API key
    public static final String API_KEY = "yourapikey";
    //ADD your dev account secret key
    public static final String SECRET_KEY = "yoursecretkey";
    //Add ur own state
    public static final String STATE = "123456789";

    //Add your redirect URI
    public static final String REDIRECT_URI = "https://www.example.com";
}


![alt text](/ScreenShots/li_1.png?raw=true "Screen 1")
![alt text](/ScreenShots/li_2.png?raw=true "Screen 2")
![alt text](/ScreenShots/li_3.png?raw=true "Screen 3")


If you face any issue, please contact me on chetanansel24@gmail.com
